﻿
namespace StockTool
{
    partial class MainToolForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FunctionList = new System.Windows.Forms.CheckedListBox();
            this.FunctionPage = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.TP1_InfoBox = new System.Windows.Forms.TextBox();
            this.TP1_AddQuoteBtn = new System.Windows.Forms.Button();
            this.TP1_GBox = new System.Windows.Forms.GroupBox();
            this.TP1_AddTextBox = new System.Windows.Forms.TextBox();
            this.TP1_Addbtn = new System.Windows.Forms.Button();
            this.TP1_Delbtn = new System.Windows.Forms.Button();
            this.TP1_Savebtn = new System.Windows.Forms.Button();
            this.TP1_StockList = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ShowInfoBox = new System.Windows.Forms.TextBox();
            this.FunctionPage.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FunctionList
            // 
            this.FunctionList.FormattingEnabled = true;
            this.FunctionList.Items.AddRange(new object[] {
            "功能一",
            "功能二"});
            this.FunctionList.Location = new System.Drawing.Point(1, 0);
            this.FunctionList.Name = "FunctionList";
            this.FunctionList.Size = new System.Drawing.Size(355, 599);
            this.FunctionList.TabIndex = 0;
            // 
            // FunctionPage
            // 
            this.FunctionPage.Controls.Add(this.tabPage1);
            this.FunctionPage.Controls.Add(this.tabPage2);
            this.FunctionPage.Location = new System.Drawing.Point(361, 0);
            this.FunctionPage.Name = "FunctionPage";
            this.FunctionPage.SelectedIndex = 0;
            this.FunctionPage.Size = new System.Drawing.Size(1291, 598);
            this.FunctionPage.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.TP1_InfoBox);
            this.tabPage1.Controls.Add(this.TP1_AddQuoteBtn);
            this.tabPage1.Controls.Add(this.TP1_GBox);
            this.tabPage1.Controls.Add(this.TP1_AddTextBox);
            this.tabPage1.Controls.Add(this.TP1_Addbtn);
            this.tabPage1.Controls.Add(this.TP1_Delbtn);
            this.tabPage1.Controls.Add(this.TP1_Savebtn);
            this.tabPage1.Controls.Add(this.TP1_StockList);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1283, 572);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // TP1_InfoBox
            // 
            this.TP1_InfoBox.Location = new System.Drawing.Point(677, 14);
            this.TP1_InfoBox.Multiline = true;
            this.TP1_InfoBox.Name = "TP1_InfoBox";
            this.TP1_InfoBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TP1_InfoBox.Size = new System.Drawing.Size(324, 547);
            this.TP1_InfoBox.TabIndex = 7;
            // 
            // TP1_AddQuoteBtn
            // 
            this.TP1_AddQuoteBtn.Location = new System.Drawing.Point(25, 405);
            this.TP1_AddQuoteBtn.Name = "TP1_AddQuoteBtn";
            this.TP1_AddQuoteBtn.Size = new System.Drawing.Size(125, 23);
            this.TP1_AddQuoteBtn.TabIndex = 6;
            this.TP1_AddQuoteBtn.Text = "註冊全部到QuoteCom";
            this.TP1_AddQuoteBtn.UseVisualStyleBackColor = true;
            this.TP1_AddQuoteBtn.Click += new System.EventHandler(this.TP1_AddQuoteBtn_Click);
            // 
            // TP1_GBox
            // 
            this.TP1_GBox.Location = new System.Drawing.Point(299, 26);
            this.TP1_GBox.Name = "TP1_GBox";
            this.TP1_GBox.Size = new System.Drawing.Size(350, 316);
            this.TP1_GBox.TabIndex = 5;
            this.TP1_GBox.TabStop = false;
            this.TP1_GBox.Text = "設定選項";
            // 
            // TP1_AddTextBox
            // 
            this.TP1_AddTextBox.Location = new System.Drawing.Point(25, 377);
            this.TP1_AddTextBox.Name = "TP1_AddTextBox";
            this.TP1_AddTextBox.Size = new System.Drawing.Size(100, 22);
            this.TP1_AddTextBox.TabIndex = 4;
            this.TP1_AddTextBox.Text = "新增股票代號";
            // 
            // TP1_Addbtn
            // 
            this.TP1_Addbtn.Location = new System.Drawing.Point(131, 377);
            this.TP1_Addbtn.Name = "TP1_Addbtn";
            this.TP1_Addbtn.Size = new System.Drawing.Size(75, 23);
            this.TP1_Addbtn.TabIndex = 3;
            this.TP1_Addbtn.Text = "新增";
            this.TP1_Addbtn.UseVisualStyleBackColor = true;
            this.TP1_Addbtn.Click += new System.EventHandler(this.Page1_Addbtn_Click);
            // 
            // TP1_Delbtn
            // 
            this.TP1_Delbtn.Location = new System.Drawing.Point(106, 348);
            this.TP1_Delbtn.Name = "TP1_Delbtn";
            this.TP1_Delbtn.Size = new System.Drawing.Size(87, 23);
            this.TP1_Delbtn.TabIndex = 2;
            this.TP1_Delbtn.Text = "刪除選定股票";
            this.TP1_Delbtn.UseVisualStyleBackColor = true;
            this.TP1_Delbtn.Click += new System.EventHandler(this.Page1_Delbtn_Click);
            // 
            // TP1_Savebtn
            // 
            this.TP1_Savebtn.Location = new System.Drawing.Point(25, 348);
            this.TP1_Savebtn.Name = "TP1_Savebtn";
            this.TP1_Savebtn.Size = new System.Drawing.Size(75, 23);
            this.TP1_Savebtn.TabIndex = 1;
            this.TP1_Savebtn.Text = "存檔";
            this.TP1_Savebtn.UseVisualStyleBackColor = true;
            this.TP1_Savebtn.Click += new System.EventHandler(this.Page1_Savebtn_Click);
            // 
            // TP1_StockList
            // 
            this.TP1_StockList.FormattingEnabled = true;
            this.TP1_StockList.ItemHeight = 12;
            this.TP1_StockList.Location = new System.Drawing.Point(25, 26);
            this.TP1_StockList.Name = "TP1_StockList";
            this.TP1_StockList.Size = new System.Drawing.Size(236, 316);
            this.TP1_StockList.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1283, 572);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ShowInfoBox
            // 
            this.ShowInfoBox.Location = new System.Drawing.Point(1, 605);
            this.ShowInfoBox.Multiline = true;
            this.ShowInfoBox.Name = "ShowInfoBox";
            this.ShowInfoBox.Size = new System.Drawing.Size(1647, 289);
            this.ShowInfoBox.TabIndex = 3;
            // 
            // MainToolForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1653, 906);
            this.Controls.Add(this.ShowInfoBox);
            this.Controls.Add(this.FunctionPage);
            this.Controls.Add(this.FunctionList);
            this.Name = "MainToolForm";
            this.Text = "MainToolForm";
            this.FunctionPage.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox FunctionList;
        private System.Windows.Forms.TabControl FunctionPage;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox ShowInfoBox;
        private System.Windows.Forms.ListBox TP1_StockList;
        private System.Windows.Forms.Button TP1_Delbtn;
        private System.Windows.Forms.Button TP1_Savebtn;
        private System.Windows.Forms.Button TP1_Addbtn;
        private System.Windows.Forms.TextBox TP1_AddTextBox;
        private System.Windows.Forms.GroupBox TP1_GBox;
        private System.Windows.Forms.Button TP1_AddQuoteBtn;
        private System.Windows.Forms.TextBox TP1_InfoBox;
    }

}

