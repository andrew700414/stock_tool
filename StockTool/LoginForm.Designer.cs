﻿namespace StockTool
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.tbUserIDNO = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbPWD = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.ckbUserIDNO = new System.Windows.Forms.CheckBox();
            this.ckbUserPWD = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbLoginMsg = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbUserIDNO
            // 
            this.tbUserIDNO.Location = new System.Drawing.Point(136, 50);
            this.tbUserIDNO.Name = "tbUserIDNO";
            this.tbUserIDNO.Size = new System.Drawing.Size(143, 22);
            this.tbUserIDNO.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "身份證號";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "交易密碼";
            // 
            // tbPWD
            // 
            this.tbPWD.Location = new System.Drawing.Point(136, 87);
            this.tbPWD.Name = "tbPWD";
            this.tbPWD.PasswordChar = '*';
            this.tbPWD.Size = new System.Drawing.Size(143, 22);
            this.tbPWD.TabIndex = 2;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(141, 141);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "登入";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // ckbUserIDNO
            // 
            this.ckbUserIDNO.AutoSize = true;
            this.ckbUserIDNO.Checked = true;
            this.ckbUserIDNO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbUserIDNO.Location = new System.Drawing.Point(70, 119);
            this.ckbUserIDNO.Name = "ckbUserIDNO";
            this.ckbUserIDNO.Size = new System.Drawing.Size(108, 16);
            this.ckbUserIDNO.TabIndex = 5;
            this.ckbUserIDNO.Text = "記住本次登入ID";
            this.ckbUserIDNO.UseVisualStyleBackColor = true;
            // 
            // ckbUserPWD
            // 
            this.ckbUserPWD.AutoSize = true;
            this.ckbUserPWD.Checked = true;
            this.ckbUserPWD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbUserPWD.Location = new System.Drawing.Point(184, 119);
            this.ckbUserPWD.Name = "ckbUserPWD";
            this.ckbUserPWD.Size = new System.Drawing.Size(120, 16);
            this.ckbUserPWD.TabIndex = 6;
            this.ckbUserPWD.Text = "記住本次登入密碼";
            this.ckbUserPWD.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(397, 41);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // lbLoginMsg
            // 
            this.lbLoginMsg.AutoSize = true;
            this.lbLoginMsg.ForeColor = System.Drawing.Color.Red;
            this.lbLoginMsg.Location = new System.Drawing.Point(76, 122);
            this.lbLoginMsg.Name = "lbLoginMsg";
            this.lbLoginMsg.Size = new System.Drawing.Size(0, 12);
            this.lbLoginMsg.TabIndex = 8;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 170);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(385, 184);
            this.textBox1.TabIndex = 9;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(397, 366);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lbLoginMsg);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ckbUserPWD);
            this.Controls.Add(this.ckbUserIDNO);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbPWD);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbUserIDNO);
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "登入";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbUserIDNO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbPWD;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.CheckBox ckbUserIDNO;
        private System.Windows.Forms.CheckBox ckbUserPWD;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbLoginMsg;
        private System.Windows.Forms.TextBox textBox1;
    }
}