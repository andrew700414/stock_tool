﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace StockTool
{
    public partial class MainToolForm : Form
    {
        FunctionClass1 Function1;
        Dictionary<string, string> StockToFuturesMap;
        public MainToolForm()
        {
            Function1 = new FunctionClass1(this);
            StockToFuturesMap = new Dictionary<string, string>();
            AnalysisStockToFuturesXml();    //讀檔要早做，後面的流程會用到
            InitializeComponent();            
            LoadToolsStockList(1, TP1_StockList);
            //股票代號對應股期編號測試
            string StockCodeStr_ = GetFuturesCodeFromStockID(2317);
            //價差Tick測試
            int Ticks_ = GlobalFunction.GetPriceCompare((float)97.5, (float)101);
            AddTP1_Info(StockCodeStr_);           
        }
        //載入並分析股期資料(提供備查)
        private void AnalysisStockToFuturesXml()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("ProList.xml");
            //獲取根節點
            XmlElement root = xmlDoc.DocumentElement;
            //獲取子節點
            XmlNodeList pnodes = xmlDoc.GetElementsByTagName("Row");
            //使用foreach讀資料
            foreach (XmlNode node in pnodes)
            {
                string StockID_ = ((XmlElement)node).GetElementsByTagName("StockNo")[0].InnerText;
                string FuturesName_ = ((XmlElement)node).GetElementsByTagName("StockCode")[0].InnerText;
                //目前因為有小型股期(Ex.大立光和小型大立光)，都會吃到同一組Key，目前暫時先避掉，之後再想辦法解決
                if(StockToFuturesMap.ContainsKey(StockID_))
                    continue;
                StockToFuturesMap.Add(StockID_, FuturesName_);
            }        
        }

        //從股票ID查出股票期貨的近月代號
        public string GetFuturesCodeFromStockID(int StockID)
        {
            if (StockToFuturesMap.ContainsKey(StockID.ToString()))
                return GlobalFunction.GetFullStockCode(StockToFuturesMap[StockID.ToString()]);                
            return "";
        }
        //確認該檔股票是否有對應股期
        public bool ChkFuturesFromStockID(int StockID)
        {
            //檢查該檔股票是否有股期，有的話才可以加入
            if (GetFuturesCodeFromStockID(StockID) == "")
            {
                string ErrText_ = "錯誤!該檔股票" + StockID.ToString() + "沒有對應的股票期貨!";
                AddTP1_Info(ErrText_);
                return false;
            }
            return true;
        }
        //讀取股票存檔資料
        public void LoadToolsStockList(byte ToolSN, ListBox StockList)
        {
            bool Result_ = true;            
            string LoadFileName_ = "StockToolList" + ToolSN.ToString() + ".txt";
            LoadFileName_ = Path.GetFullPath(LoadFileName_);
            StreamReader sr = new StreamReader(LoadFileName_);
            // 每次讀取一行，直到檔尾
            while (!sr.EndOfStream)
            {
                int StockID_;
                string line = sr.ReadLine();            // 讀取文字到 line 變數
                // 股票代號都是四碼
                Result_ = Int32.TryParse(line.Substring(0, 4), out StockID_);
                if (Result_ == false)
                {
                    ShowInfoBox.AppendText(StockList.Name + " 取得股價列表資訊錯誤!\n");
                    break;
                }
                if (!ChkFuturesFromStockID(StockID_)) continue;
                StockList.Items.Add(line);                
            }
            sr.Close();

            if (Result_)
            {
                ShowInfoBox.AppendText(StockList.Name + " 讀檔成功\n");
            }
        }

        public void SaveToolsStockList(byte ToolSN, ListBox StockList)
        {
            string SaveFileName_ = "StockToolList" + ToolSN.ToString() + ".txt";
            SaveFileName_ = Path.GetFullPath(SaveFileName_);
            StreamWriter sw = new StreamWriter(SaveFileName_);
            int ItemNum_ = StockList.Items.Count;
            for (int i = 0; i < ItemNum_; ++i)
            {
                if (StockList.Items[i] != "")
                    sw.WriteLine(StockList.Items[i]);            // 寫入資料
                else
                    break;
            }            
            sw.Close();                     // 關閉串流
            ShowInfoBox.AppendText(StockList.Name + " 存檔成功\n");
        }

        public void AddToolsStockList(int StockID, ListBox StockList)
        {
            StockList.Items.Add(StockID);
            ShowInfoBox.AppendText(StockList.Name + " 新增成功\n");
        }

        public void DelToolsStockList(int SelectIndex, ListBox StockList)
        {
            StockList.Items.RemoveAt(SelectIndex);
            ShowInfoBox.AppendText(StockList.Name + " 刪除成功\n");
        }

        private void Page1_Delbtn_Click(object sender, EventArgs e)
        {
            DelToolsStockList(TP1_StockList.SelectedIndex, TP1_StockList);
        }

        private void Page1_Savebtn_Click(object sender, EventArgs e)
        {
            SaveToolsStockList(1, TP1_StockList);
        }

        private void Page1_AddTextBox_TextClick(object sender, EventArgs e)
        {
            TP1_AddTextBox.Text = "";
        }

        private void Page1_Addbtn_Click(object sender, EventArgs e)
        {
            int StockID_ = int.Parse(TP1_AddTextBox.Text);
            if (!ChkFuturesFromStockID(StockID_)) return;
            AddToolsStockList(StockID_, TP1_StockList);
        }

        private void TP1_AddQuoteBtn_Click(object sender, EventArgs e)
        {
            Function1.AddQuoteStockList(TP1_StockList);
        }

        private delegate void D_AddTP1_Info(string Text);  
        public void AddTP1_Info(string Text)
        {
            Text = Text + "\n";
            if (this.InvokeRequired)
            {
                D_AddTP1_Info uu = new D_AddTP1_Info(AddTP1_Info);
                this.Invoke(uu, Text);
            }
            else
            {
                TP1_InfoBox.AppendText(Text);
            }  
        }
    }
}
