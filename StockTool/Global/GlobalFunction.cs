﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Package;
using Intelligence;
using EnumSet;

namespace EnumSet
{
    public enum eRegistType
    {
        RT_Match,       //成交
        RT_Depth       //五檔
    }
};

struct GlobalFunction
{
    //取得數字中的某一個位數(n=0即取個位數，n=1即取十位數，依此類推)
    private static int digit(int number, int n)
    {
        return (int)(number / Math.Pow(10, n) % 10);
    }
    //確認目前期貨月份(包含股期)
    private static int GetFuturesMonth()
    {
        int iWeek_ = getWeekNumInMonth(DateTime.Now);
        DateTime dt = DateTime.Now;
        //每月第三個星期三後，期貨近月+1個月
        if (iWeek_ == 3 && dt.DayOfWeek > DayOfWeek.Wednesday)
        {   
            //跨年了
            if (dt.Month == 12)
                return 1;
            else
                return dt.Month + 1;
        }
        else
            return dt.Month;
    }
    //取得凱基股期月份代號(參數為近月編號)
    private static string GetFuturesMonthName(int MonthID)
    {
        string TempStr_ = "A";
        int iChar_ = Convert.ToInt32(TempStr_[0]) + (MonthID - 1);
        char cResult_ = Convert.ToChar(iChar_);
        return cResult_.ToString();
    }
    //從目前股價價位取得每一個Tick的變動價位
    private static float GetPricePerTick(float Price)
    {
        if (Price < 10)
            return (float)0.01;
        else if (Price < 50 && Price >= 10)
            return (float)0.05;
        else if (Price < 100 && Price >= 50)
            return (float)0.1;
        else if (Price < 500 && Price >= 100)
            return (float)0.5;
        else if (Price < 1000 && Price >= 500)
            return (float)1;
        else if (Price >= 1000)
            return (float)5;
        else
            return 0;
    }
    //取得股價最接近的臨界區間，依序為10,50,100,500,1000(Ex.股價105，臨界區間為100)
    private static float GetCriticalPrice(float Price)
    {
        if (Price < 10)
            return (float)10;
        else if (Price < 50 && Price >= 10)
        {
            //比較靠近10元價位
            if(49-Price > Price-10)
                return (float)10;
            else
                return (float)50;
        }
        else if (Price < 100 && Price >= 50)
        {
            //比較靠近50元價位
            if (99 - Price > Price - 50)
                return (float)50;
            else
                return (float)100;
        }
        else if (Price < 500 && Price >= 100)
        {
            //比較靠近100元價位
            if (499 - Price > Price - 100)
                return (float)100;
            else
                return (float)500;
        }
        else if (Price < 1000 && Price >= 500)
        {
            //比較靠近500元價位
            if (999 - Price > Price - 500)
                return (float)500;
            else
                return (float)1000;
        }
        else if (Price >= 1000)
            return (float)1000;
        else
            return 0;
    }
    //取得填入的時間是該月的哪一週
    private static int getWeekNumInMonth(DateTime daytime)
    {
        int dayInMonth = daytime.Day;
        //本月第一天
        DateTime firstDay = daytime.AddDays(1 - daytime.Day);
        //本月第一天是周幾
        int weekday = (int)firstDay.DayOfWeek == 0 ? 7 : (int)firstDay.DayOfWeek;
        //本月第一周有幾天
        int firstWeekEndDay = 7 - (weekday - 1);
        //當前日期和第一周之差
        int diffday = dayInMonth - firstWeekEndDay;
        diffday = diffday > 0 ? diffday : 1;
        //當前是第幾周,如果整除7就減一天
        int WeekNumInMonth = ((diffday % 7) == 0
         ? (diffday / 7 - 1)
         : (diffday / 7)) + 1 + (dayInMonth > firstWeekEndDay ? 1 : 0);
        return WeekNumInMonth;
    }
    //QuoteCom傳過來的日期(格式:20180604)
    private static string ConvertQuoteComDate(string QuoteComDate)
    {
        string TmpStr1_ = QuoteComDate.Insert(6, "-");
        string TmpStr2_ = TmpStr1_.Insert(4, "-");
        return TmpStr2_;
    }
    //計算距離到期日的剩餘天數
    public static int CountLeastDay(string ExpireDate)
    {
        string TmpDate_ = ConvertQuoteComDate(ExpireDate);
        DateTime myDate = DateTime.Now;
        string myDateString = myDate.ToString("yyyy-MM-dd");
        DateTime sDate = Convert.ToDateTime(myDateString);
        DateTime eDate = Convert.ToDateTime(TmpDate_);
        TimeSpan ts = eDate - sDate;
        double days = ts.TotalDays;
        return (int)days;
    }
    //增加商品列表(多檔註冊)
    public static string AddItemStr(string TotalStr, string AddItem)
    {
        int TotalLen_ = TotalStr.Length;
        int AddLen_ = AddItem.Length;
        string TmpStr_;
        if (TotalLen_ == 0)
            return AddItem;
        TmpStr_ = TotalStr.Insert(TotalLen_, "|");    //先加上|
        return TmpStr_.Insert(TotalLen_ + 1, AddItem);        
    }
    //從字串裡取StockID
    //(沒有權證)
    public static int GetStockIDFromStr(string InputStr)    //(完整回傳字串)
    {
        int StockID_ = 0;
        string TmpStr_ = "";
        if (string.Compare(InputStr, "TW.MATCH.") > 0)
        {
            TmpStr_ = "TW.MATCH.";
            // 股票代號都是四碼
            if(InputStr.Length == TmpStr_.Length + 4)
            Int32.TryParse(InputStr.Substring(TmpStr_.Length, 4), out StockID_);
        }
        return StockID_;
    }
    //註冊證券商品資料
    public static short RegStockToQuote(QuoteCom RegistQuote, string StockList, eRegistType Type)
    {
        if (Type == eRegistType.RT_Match)
            return RegistQuote.SubQuotesMatch(StockList);
        else if (Type == eRegistType.RT_Depth)
            return RegistQuote.SubQuotesDepth(StockList);
        else
            return 0;
    }
    //取消註冊證券商品資料
    public static void UnRegStockFromQuote(QuoteCom RegistQuote, string StockList, eRegistType Type)
    {
        if (Type == eRegistType.RT_Match)
            RegistQuote.UnSubQuotesMatch(StockList);
        else if (Type == eRegistType.RT_Depth)
            RegistQuote.UnSubQuotesDepth(StockList);
    }
    //註冊期貨商品資料
    public static short RegFuturesToQuote(QuoteCom RegistQuote, string FuturesList)
    {
        return RegistQuote.SubQuotes(FuturesList);
    }
    //取消註冊期貨商品資料
    public static void UnRegFuturesFromQuote(QuoteCom RegistQuote, string FuturesList)
    {
        RegistQuote.UnsubQuotes(FuturesList);
    }
    //用StockID取得股票名稱
    public static string GetStockNameFromID(QuoteCom SearchQuote, int StockID)
    {
        PI30001 i30001 = SearchQuote.GetProductSTOCK(StockID.ToString());
        if (i30001 == null)
        {
            return "";
        }
        return i30001.StockName;
    }
    //用FuturesID取得期貨名稱
    public static string GetFuturesNameFromID(QuoteCom SearchQuote, int StockID)
    {
        PI20008 i20008 = SearchQuote.GetProdcutBase(StockID.ToString());
        if (i20008 == null)
        {
            return "";
        }
        return i20008._PROD_NAME;
    }
    //查詢兩個價錢之間相差的Tick數(回傳Tick)
    public static int GetPriceCompare(float Price1, float Price2)
    {
        float PerTick_ = 0, PerTick2_ = 0,CriticalPrice_ = 0;
        int ReturnTick_ = 0;
        //先確認價格是否在同一區間
        //如果是同一區間就相對單純
        if (GetPricePerTick(Price1) == GetPricePerTick(Price2))
        {
            PerTick_ = GetPricePerTick(Price1);
            ReturnTick_ = (short)((Price1 - Price2) / PerTick_);
            return Math.Abs(ReturnTick_);
        }
        //不同區間的算法
        if (GetPricePerTick(Price1) != GetPricePerTick(Price2))
        {
            //理論上不可能發生
            if (GetCriticalPrice(Price1) != GetCriticalPrice(Price2))
                return 0;
            CriticalPrice_ = GetCriticalPrice(Price1);
            PerTick_ = GetPricePerTick(Price1);
            PerTick2_ = GetPricePerTick(Price2);
            if (Price1 > Price2)
            {
                return Convert.ToInt16((((Price1 - CriticalPrice_) / PerTick_) + ((CriticalPrice_ - Price2) / PerTick2_)));
            }
            else if (Price2 > Price1)
            {                
                return Convert.ToInt16((((Price2 - CriticalPrice_) / PerTick2_) + ((CriticalPrice_ - Price1) / PerTick_)));
            }
        }

        return 0;
    }
    //取得股期完整近月代號(輸入股期代碼 例鴻海=DH)
    public static string GetFullStockCode(string StockCode)
    {
        DateTime dt = DateTime.Now;
        int FuturesYear_;   //期貨年分
        int NearMonth_ = GetFuturesMonth();
        //年份跨年了
        if(dt.Month != NearMonth_ && dt.Month == 12 && NearMonth_ == 1)
            FuturesYear_ = dt.Year + 1;
        else
            FuturesYear_ = dt.Year;

        int YearStr_ = digit(FuturesYear_, 0);
         
        //股期規則=股期代碼+"F"+月分代碼+年份尾數
        return StockCode + "F" + GetFuturesMonthName(NearMonth_) + YearStr_.ToString();               
    }
}
